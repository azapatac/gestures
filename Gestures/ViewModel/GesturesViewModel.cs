﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using Gestures.Views;
using Xamarin.Forms;

namespace Gestures.ViewModel
{
    public class GesturesViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public ICommand DoubleTapCommand { get; set; }
        public ICommand SwipeCommand { get; set; }
        public ICommand TapCommand { get; set; }

        public GesturesViewModel()
        {
            DoubleTapCommand = new Command(ExecuteDoubleTapCommand);    
            SwipeCommand = new Command<string>((direction) => ExecuteSwipeCommand(direction));
            TapCommand = new Command(ExecuteTapCommand);
        }

        private void ExecuteDoubleTapCommand(object obj)
        {
            App.Current.MainPage.Navigation.PushAsync(new PanGesturePage());
        }

        private void ExecuteSwipeCommand(string direction)
        {
            _ = App.Current.MainPage.DisplayAlert(".Net University", $"TabCommand {direction}", "Ok");
        }

        private void ExecuteTapCommand(object obj)
        {
            App.Current.MainPage.Navigation.PushAsync(new PinchGesturePage());    
        }

        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        protected bool SetField<T>(ref T field, T value, [CallerMemberName] string propertyName = null)
        {
            if (EqualityComparer<T>.Default.Equals(field, value))
                return false;

            field = value;
            OnPropertyChanged(propertyName);

            return true;
        }

    }
}
