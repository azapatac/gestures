﻿using System.ComponentModel;
using Gestures.ViewModel;
using Xamarin.Forms;

namespace Gestures
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
		

        public MainPage()
        {
            InitializeComponent();
            BindingContext = new GesturesViewModel();
        }
	}
}
