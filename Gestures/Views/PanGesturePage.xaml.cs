﻿using System;

using Xamarin.Forms;

namespace Gestures.Views
{
    public partial class PanGesturePage : ContentPage
    {
        double x, y;

        public PanGesturePage()
        {
            InitializeComponent();
        }

		//https://github.com/xamarin/xamarin-forms-samples/tree/master/WorkingWithGestures/PanGesture
		void OnPanUpdated(object sender, PanUpdatedEventArgs e)
		{
			switch (e.StatusType)
			{
				case GestureStatus.Running:
					// Translate and ensure we don't pan beyond the wrapped user interface element bounds.
					Content.TranslationX =
					  Math.Max(Math.Min(0, x + e.TotalX), -Math.Abs(Content.Width - App.Current.MainPage.Width));
					Content.TranslationY =
					  Math.Max(Math.Min(0, y + e.TotalY), -Math.Abs(Content.Height - App.Current.MainPage.Height));
					break;

				case GestureStatus.Completed:
					// Store the translation applied during the pan
					x = Content.TranslationX;
					y = Content.TranslationY;
					break;
			}
		}
	}
}
